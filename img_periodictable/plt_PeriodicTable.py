#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import copy
from pymatgen.core.periodic_table import Element


class plt_PeriodicTable:
    """show selected elements in the periodic table by using matplotlib.plt.table."""

    def __init__(
        self,
        cindex,
        default_background_color="w",
        select_color="red",
        font_color="black",
        font_size=8,
        select_fontcolor="black",
        group_index=False,
        group_index_color="black",
        group_index_fontcolor="white",
    ):
        """initialization

        Args:
            cindex (List): a list of Z(int)
            default_background_color (str, optional): default background color. Defaults to "w".
            select_color (str, optional): selected background color. Defaults to "red".
            font_color (str, optional): font color. Defaults to "black".
            font_size (int, optional): font size. Defaults to 8.
            select_fontcolor (str, optional): selected font color. Defaults to "black".
            group_index (bool, optional): show group index or not. Defaults to False.
            group_index_color (str, optional): group index background color. Defaults to "black".
            group_index_fontcolor (str, optional): group index font color. Defaults to "white".
        """
        cindex = self.modify_cindex(cindex)
        print(cindex)

        self.make_setting(
            cindex,
            default_background_color,
            select_color,
            font_color,
            font_size,
            select_fontcolor,
            group_index,
            group_index_color,
            group_index_fontcolor,
        )

    def modify_cindex(self, cindex):
        """
        string to integer
        """
        cindex_ = []
        for c in cindex:
            if isinstance(c, str):
                elm = Element(c)
                c = elm.Z
            cindex_.append(c)
        return cindex_

    def make_setting(
        self,
        cindex,
        default_background_color,
        select_color,
        font_color,
        font_size,
        select_fontcolor,
        group_index,
        group_index_color,
        group_index_fontcolor,
    ):
        """make internal variables

        Args:
            cindex (List): a list of Z(int)
            default_background_color (str, optional): default background color.
            select_color (str, optional): selected background color.
            font_color (str, optional): font color.
            font_size (int, optional): font size.
            select_fontcolor (str, optional): selected font color.
            group_index (bool, optional): show group index or not.
            group_index_color (str, optional): group index background color.
            group_index_fontcolor (str, optional): group index font color.
        """

        self.font_color = font_color
        self.group_index_fontcolor = group_index_fontcolor
        self.font_size = font_size
        ptable = []
        if group_index:
            nrow = 11
            row_sub = 0
        else:
            nrow = 10
            row_sub = 1
        for i in range(nrow):
            v = []
            for j in range(18):
                v.append("              ")
            ptable.append(v)
        ptable = np.array(ptable)
        ctable = copy.deepcopy(ptable)
        ctable[:, :] = default_background_color
        ftable = copy.deepcopy(ptable)
        ftable[:, :] = font_color

        if group_index:
            for g in range(18):
                ptable[0, g] = g + 1
                ctable[0, g] = group_index_color
                ftable[0, g] = group_index_fontcolor
        for z in range(1, 118):
            elm = Element.from_Z(z)
            row, group = elm.row, elm.group
            if z >= 57 and z <= 71:
                row = 9
                group = z - 57 + 3
            if z >= 89 and z <= 103:
                row = 10
                group = z - 89 + 3

            ptable[row - row_sub, group - 1] = elm.symbol
            if z in cindex:
                ctable[row - row_sub, group - 1] = select_color
                ftable[row - row_sub, group - 1] = select_fontcolor
            elif elm.is_lanthanoid:
                ctable[row - row_sub, group - 1] = "darkkhaki"
            elif elm.is_actinoid:
                ctable[row - row_sub, group - 1] = "palegoldenrod"
            elif elm.is_alkali:
                ctable[row - row_sub, group - 1] = "cornflowerblue"
            elif elm.is_alkaline:
                ctable[row - row_sub, group - 1] = "skyblue"
            elif elm.is_noble_gas:
                ctable[row - row_sub, group - 1] = "lightgray"
            elif elm.is_transition_metal:
                ctable[row - row_sub, group - 1] = "lightgreen"
            elif elm.is_halogen:
                ctable[row - row_sub, group - 1] = "coral"
            elif elm.is_metalloid:
                ctable[row - row_sub, group - 1] = "orange"
            elif elm.is_post_transition_metal:
                ctable[row - row_sub, group - 1] = "gold"
            else:
                ctable[row - row_sub, group - 1] = "lightpink"
        self.cindex = cindex
        self.ptable = ptable
        self.ctable = ctable
        self.ftable = ftable

    def set_table(self, ax):
        """draw ax of matplotlib.plt

        Args:
            ax (matplotlib.plt.ax): axis of matplotlib
        """
        ax.axis("tight")
        ax.axis("off")

        ptable = self.ptable
        ctable = self.ctable
        table = ax.table(cellText=ptable, cellColours=ctable, loc="upper center")

        table.auto_set_font_size(False)
        table.set_fontsize(self.font_size)

        for i in range(ptable.shape[0]):
            for j in range(ptable.shape[1]):
                table[(i, j)].get_text().set_color(self.ftable[i, j])
