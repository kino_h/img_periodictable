#!/usr/bin/env python
# coding: utf-8
import matplotlib.pyplot as plt
import sys
from img_periodictable import plt_PeriodicTable

def addpath():
	addpath = ".."
	sys.path.append(addpath)


def test1(filename="test1.png"):
    """test 1 
    background is white.
    """
    cindex = [13, 14, 26, 57, 27, 60]
    pctable = plt_PeriodicTable(cindex, default_background_color="w",
                                select_color="r", select_fontcolor="b",
                                group_index_color="w", group_index_fontcolor="black",
                                group_index=True)
    fig, ax = plt.subplots(figsize=(6, 6), dpi=100)
    pctable.set_table(ax)
    fig.savefig(filename)
    fig.show()

test1()

def test2(filename="test2.png"):
    """test2
    backgropund is black.
    """
    cindex = [13, 14, 26, 57, 27]
    pctable = plt_PeriodicTable(cindex, default_background_color="black",
                                select_color="blue", select_fontcolor="white",
                                group_index_color="black", group_index_fontcolor="white",
                                group_index=True)
    fig, ax = plt.subplots(figsize=(6, 6), dpi=100)
    pctable.set_table(ax)
    fig.savefig(filename)
    fig.show()

test2()
